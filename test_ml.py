import pandas as pd
import numpy as np
import math

from collections import Counter
from statistics import mean
from sklearn.cross_validation import LeaveOneOut
from sklearn.ensemble import RandomForestClassifier
from sklearn.decomposition import PCA
from sklearn.cross_decomposition import PLSCanonical
from sklearn.metrics import log_loss
from sklearn.preprocessing import StandardScaler, Normalizer
from xgboost import XGBClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression


def gen_out_string(ids, probs):
    print('Id,Prob1,Prob2')
    for id, line in zip(ids, probs):
        l = ','.join(str(x) for x in line)
        print(f'{id},{l}')

def calc_log_loss(probs):
    probs_real = pd.read_csv('best.csv').as_matrix()
    return log_loss(probs_real, probs)

def scale(X, X_test):
    columns = X.columns
    X_ = pd.concat([X, X_test])
    scaler = StandardScaler()
    scaler.fit(X_)
    X_scaled, X_test_scaled = scaler.transform(X), scaler.transform(X_test)
    X_scaled = pd.DataFrame(X_scaled, columns=columns)
    X_test_scaled = pd.DataFrame(X_test_scaled, columns=columns)
    return X_scaled, X_test_scaled

def normalize(X, X_test):
    columns = X.columns
    X_ = pd.concat([X, X_test])
    normalizer = Normalizer()
    normalizer.fit(X_)
    X_scaled, X_test_scaled = normalizer.transform(X), normalizer.transform(X_test)
    X_scaled = pd.DataFrame(X_scaled, columns=columns)
    X_test_scaled = pd.DataFrame(X_test_scaled, columns=columns)
    return X_scaled, X_test_scaled

def convert_pca(X, X_test, n_components):
    pca = PCA(n_components=n_components)
    pca.fit(X)
    X_PCA = pca.transform(X)
    X_test_PCA = pca.transform(X_test)
    X_PCA = pd.DataFrame(X_PCA)
    X_test_PCA = pd.DataFrame(X_test_PCA)
    return X_PCA, X_test_PCA

def convert_pls(X, Y, X_test, n_components):
    pls = PLSCanonical(n_components=n_components)
    pls.fit(X, Y)
    X_PLS = pls.transform(X)
    X_test_PLS = pls.transform(X_test)
    X_PLS = pd.DataFrame(X_PLS)
    X_test_PLS = pd.DataFrame(X_test_PLS)
    return X_PLS, X_test_PLS

def get_train_test_ndarrays():
    df = pd.read_csv('saulinho_da_garotada.csv')
    X, Y = df.iloc[:, 1:-1], df.iloc[:, -1]
    X.iloc[34, 1941] = X.iloc[:, 1941].mean()
    df_test = pd.read_csv('test.csv')
    ids = df_test.iloc[:, 0]
    X_test = df_test.iloc[:, 1:]
    X_test.iloc[20, 1941] = X_test.iloc[:, 1941].mean()
    #X_test = X_test.loc[:, X.columns]
    return X, Y, X_test, ids

def feature_selection(X, Y, X_test):
    rf = RandomForestClassifier(n_estimators=1000, criterion="entropy")
    rf.fit(X, Y)
    min_ = mean(rf.feature_importances_)
    features = [i for i, imp in enumerate(rf.feature_importances_) if imp > min_]

    return X.iloc[:, features], X_test.iloc[:, features]

X, Y, X_test, ids = get_train_test_ndarrays()
#(features X3357 to X3433)
X = X.iloc[:, 3355:3432]
X_test = X_test.iloc[:, 3356:3433]

# X, X_test = scale(X, X_test)
# X, X_test = normalize(X, X_test)

# X, X_test = feature_selection(X, Y, X_test)
# len(X.columns)
X_PCA, X_test_PCA = convert_pca(X, X_test, n_components=2)
# X_PLS, X_test_PLS = convert_pls(X, Y, X_test, n_components=20)
#X, X_test = feature_selection(X_PLS, Y, X_test_PLS)

#rf = RandomForestClassifier(n_estimators=1000)
#rf = rf.fit(X_PCA, Y)
#probs = rf.predict_proba(X_test_PCA)

logistic_reg = LogisticRegression(max_iter=1000000000)
logistic_reg = logistic_reg.fit(X_PCA, Y)
probs = logistic_reg.predict_proba(X_test_PCA)
#svm = SVC(kernel='linear', probability=True)
#svm = svm.fit(X_PCA, Y)
#probs = svm.predict_proba(X_test_PCA)
#xgb = XGBClassifier(max_depth=2, n_estimators=1000, learning_rate=0.05)
#xgb = xgb.fit(X, Y)
#probs = xgb.predict_proba(X_test)
calc_log_loss(probs)
gen_out_string(ids, probs)
'''
losses = []
for train_index, test_index in LeaveOneOut(len(X)):
    rf = RandomForestClassifier(n_estimators=1000, criterion="entropy")
    X_train, X_test = X.iloc[train_index, :], X.iloc[test_index, :]
    Y_train, Y_test = Y[train_index], Y[test_index]
    rf.fit(X_train, Y_train)
    Y_test_hat = rf.predict_proba(X_test)
    if Y_test.values[0] == 0:
        Y_test = np.array([1.0, 0.0])
    else:
        Y_test = np.array([0.0, 1.0])
    l = log_loss(Y_test, Y_test_hat[0])
    losses.append(l)
'''
